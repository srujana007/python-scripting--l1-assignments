def isWhiteLine(str1):
    spaces = str1.count(' ')
    tabs = str1.count('\t')
    if spaces+tabs == len(str1):
        return True
    else:
        return False

str1 = """The practice of writing paragraphs is essential to good writing.
				        
Paragraphs help to break up large chunks of text and makes the content easier for readers to digest.
				       
They guide the reader through your argument by focusing on one main idea or goal.
		  	 		  	
However, knowing how to write a good, well-structured paragraph can be little tricky.
     	 	 	 	 	 	
Read the guidelines below and learn how to take your paragraph writing skills from good to great!"""

for i in str1.split('\n'):
    if isWhiteLine(i)==True:
        pass
    else:
        print(i)
